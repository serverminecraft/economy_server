package com.gitlab.survivalRaiz.economy;

import api.skwead.commands.CommandManager;
import api.skwead.commands.ConfigCommand;
import api.skwead.messages.chat.placeholders.PlaceholderComponent;
import api.skwead.messages.chat.placeholders.Placeholders;
import com.gitlab.survivalRaiz.core.Core;
import com.gitlab.survivalRaiz.economy.commands.GiveMoneyCmd;
import com.gitlab.survivalRaiz.economy.commands.ShopCmd;
import com.gitlab.survivalRaiz.economy.commands.player.BalanceCmd;
import com.gitlab.survivalRaiz.economy.commands.staff.WithdrawMoneyCmd;
import com.gitlab.survivalRaiz.economy.events.LoginListener;
import com.gitlab.survivalRaiz.economy.events.TransactionListener;
import com.gitlab.survivalRaiz.economy.shop.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class EconomyPlugin extends JavaPlugin {

    private Economy economy;
    private Core core;
    private EconomyConfig economyConfig;
    private SignFormatConfig signFormatConfig;
    private ShopManager shopManager;

    @Override
    public void onEnable() {
        if (!setupCore())
            System.out.println("!    FUDEU CORE");

        this.economy = new Economy(this.core.getDbManager());

        setupEconomy();

        this.signFormatConfig = new SignFormatConfig(this);

        setupEvents();

        this.shopManager = new ShopManager(this);

        setupPlaceholders();

        setupCommands();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void setupEconomy() {
        economyConfig = new EconomyConfig(this);
    }

    private void setupEvents() {
        new LoginListener(this);
        new ShopListener(this);
        new TransactionListener(this);
    }

    private void setupCommands() {
        final Set<ConfigCommand> cmds = new HashSet<>();
        final CommandManager cm = new CommandManager(this);

        cmds.add(new GiveMoneyCmd(this));
        cmds.add(new BalanceCmd(this));
        cmds.add(new WithdrawMoneyCmd(this));
        cmds.add(new ShopCmd(this));

        try {
            cm.getConfig().generateFrom(cmds);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean setupCore() {
        if (Bukkit.getPluginManager().getPlugin("Core") == null)
            return false;

        this.core = (Core) Bukkit.getPluginManager().getPlugin("Core");
        return true;
    }

    private void setupPlaceholders() {
        final Placeholders placeholderManager = new Placeholders(this);

        placeholderManager.registerPlaceholder(new PlaceholderComponent(
                "sr_player_balance",
                ((plugin, player) -> String.valueOf(((EconomyPlugin) plugin).getEconomy().getBalance(player.getUniqueId())))
        ));
    }

    public Economy getEconomy() {
        return economy;
    }

    public Core getCore() {
        return core;
    }

    public EconomyConfig getEconomyConfig() {
        return economyConfig;
    }

    public SignFormatConfig getSignFormatConfig() {
        return signFormatConfig;
    }

    public ShopManager getShopManager() {
        return shopManager;
    }
}
