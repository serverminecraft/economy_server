package com.gitlab.survivalRaiz.economy.shop;

import api.skwead.storage.file.yml.YMLConfig;
import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

/**
 * Transaction tax calculated by <em>a + bx</em>
 * - a is the base tax
 * - b is the percentage modifier
 * - x is the percentage of all shops that sell the same item
 * <p>
 * Shop creation fee is calculated by <em>a + be^x</em>
 * - a is the base price
 * - b is the multiplier
 * - x is the number of shops the user has
 */
public class EconomyConfig extends YMLConfig {
    private final EconomyPlugin plugin;

    public EconomyConfig(EconomyPlugin plugin) {
        super("economy", plugin);

        this.plugin = plugin;

        final Set<String> headers = new HashSet<>();

        headers.add("tax_base");
        headers.add("tax_multiplier");

        headers.add("shop_base");
        headers.add("shop_multiplier");

        try {
            this.createConfig(headers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
        for (String param : set) {
            if (super.getConfig().get(param) == null) {
                super.getConfig().createSection(param);
                super.getConfig().set(param, 0.0);
                super.getConfig().save(super.getFile());
            }
        }
    }

    public String hash(ItemStack itemStack) {
        final String material = itemStack.getType().name();
        final Enchantment[] enchs = itemStack.getEnchantments().keySet().toArray(new Enchantment[0]);

        final String[] enchantments = new String[enchs.length];
        for (int i = 0; i < enchs.length; enchantments[i] = enchs[i++].toString()) ;

        final StringBuilder data = new StringBuilder();

        data.append(material);

        for (int i = 0; i < enchantments.length; data.append(enchantments[i++])) ;

        try {
            final String[] lore = itemStack.getItemMeta().getLore().toArray(new String[0]);

            for (int i = 0; i < lore.length; data.append(lore[i++])) ;
        } catch (NullPointerException e) {
            return encrypt(data.toString());
        }

        return encrypt(data.toString());
    }

    // https://howtodoinjava.com/java/java-security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/#Java%20SHA%20Hashing%20Example
    private String encrypt(String content/*, byte[] salt*/) {
        String hash = null;
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
//            md.update(salt);
            final byte[] bytes = md.digest(content.getBytes());
            final StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.length;
                 sb.append(Integer.toString((bytes[i++] & 0xff) + 0x100, 16).substring(1)));

            hash = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash;
    }

    public double getTaxBase() {
        return super.getConfig().getDouble("tax_base");
    }

    public double getTaxMultiplier() {
        return super.getConfig().getDouble("tax_multiplier");
    }

    public double getShopBase() {
        return super.getConfig().getDouble("shop_base");
    }

    public double getShopMultiplier() {
        return super.getConfig().getDouble("shop_multiplier");
    }
}
