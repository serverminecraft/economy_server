package com.gitlab.survivalRaiz.economy.shop;

import com.gitlab.survivalRaiz.core.db.DBManager;

import java.sql.SQLException;
import java.util.Collections;
import java.util.UUID;

public class Economy {

    private final DBManager dbManager;

    public Economy(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    public double getBalance(UUID player) {
        try {
            return (double) this.dbManager.getBalance(player, resultSet -> {
                try {
                    return Collections.singletonList(resultSet.getDouble(1));
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
                return Collections.singletonList(0.0);
            }).get(0);
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public boolean has(UUID player, double val) {
        return getBalance(player) >= val;
    }

    public void depositPlayer(UUID player, double val) {
        this.dbManager.setBalance(player, this.getBalance(player) + val);
    }

    public void withdrawPlayer(UUID player, double val) {
        depositPlayer(player, -val);
    }

    public void create(UUID player) {
        this.dbManager.createAcc(player);
    }
}
