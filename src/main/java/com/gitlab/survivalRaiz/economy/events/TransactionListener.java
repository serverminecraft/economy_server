package com.gitlab.survivalRaiz.economy.events;

import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import com.gitlab.survivalRaiz.economy.shop.SignFormatConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.UUID;

import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;

public class TransactionListener implements Listener {

    private final EconomyPlugin plugin;

    public TransactionListener(EconomyPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onTransaction(PlayerInteractEvent e) {
        final Block signBlock = e.getClickedBlock();

        if (signBlock == null)
            return;

        if (signBlock.getType() != SignFormatConfig.SIGN_MATERIAL)
            return;

        if (!plugin.getCore().getDbManager().isShop(signBlock.getX(), signBlock.getY(), signBlock.getZ()))
            return;

        e.setCancelled(true);

        final boolean isAdmin =
                Arrays.stream(((Sign) signBlock.getState()).getLines()).anyMatch(s -> s.contains("SERVIDOR"));

        Inventory inv_from; // where the items come from
        Inventory inv_to; // where the items go to
        UUID from; // who sends money in change for items
        UUID to; // who receives items in change for money
        double price;
        final String itemStack = plugin.getCore().getDbManager().getItemHash(signBlock.getLocation());
        final int amount = plugin.getCore().getDbManager().getAmount(signBlock.getLocation());

        switch (e.getAction()) {
            case LEFT_CLICK_BLOCK: // sell
                from = plugin.getCore().getDbManager().getShopOwner(signBlock.getLocation());
                to = e.getPlayer().getUniqueId();

                inv_from = e.getPlayer().getInventory();
                inv_to = getChest(signBlock);

                price = plugin.getCore().getDbManager().getSellPrice(signBlock.getLocation());

                break;
            case RIGHT_CLICK_BLOCK: // buy
                to = plugin.getCore().getDbManager().getShopOwner(signBlock.getLocation());
                from = e.getPlayer().getUniqueId();

                inv_to = e.getPlayer().getInventory();
                inv_from = getChest(signBlock);

                price = plugin.getCore().getDbManager().getBuyPrice(signBlock.getLocation());

                break;
            default:
                e.setCancelled(false);
                return;
        }

        if (price == -1)
            return;

        // Test money amount
        if (!plugin.getEconomy().has(from, price))
            return;

        // Test item amount
        if (!has(inv_from, itemStack, amount) /*|| !isAdmin*/)
            return;

        // Do the item transaction
        try {
            give(inv_from, inv_to, amount, isAdmin, e.getAction() == RIGHT_CLICK_BLOCK);
        } catch (ArrayIndexOutOfBoundsException exp) {
            return;
        }
        System.out.println("here");

        // Do the money transaction
        plugin.getEconomy().withdrawPlayer(from, price);
        plugin.getEconomy().depositPlayer(to, price);

        // TODO: 12/26/20 Fix this when the new scoreboard API is implemented
        plugin.getCore().reloadScoreboard(Bukkit.getPlayer(from));
        plugin.getCore().reloadScoreboard(Bukkit.getPlayer(to));
    }

    private void give(Inventory inv_from, Inventory inv_to, int amount, boolean isAdmin, boolean shopIsSelling) {
        final ItemStack ref = shopIsSelling ?
                plugin.getShopManager().removeNull(inv_from)[0].clone() :
                plugin.getShopManager().removeNull(inv_to)[0].clone();
        ref.setAmount(1);

        final ItemStack[] inv = plugin.getShopManager().removeNull(inv_from);
        int j = 0;
        while (j < inv.length) {
            if (plugin.getEconomyConfig().hash(inv[j]).equals(plugin.getEconomyConfig().hash(ref)))
                break;

            j++;
        }

        final ItemStack toGive = inv[j].clone();
        toGive.setAmount(1);

        for (int i = amount; i > 0; i--) {
            System.out.println("REMOVENDO: " + i + " - " + isAdmin);
            if (isAdmin) {
                if (shopIsSelling)
                    inv_to.addItem(toGive);
                else
                    inv_from.removeItem(toGive);
            } else {
                System.out.println("add");
                inv_to.addItem(toGive);
                System.out.println("rem");
                inv_from.removeItem(toGive);
            }
        }
    }

    private boolean has(Inventory inv, String s, int amount) {
        int actual = 0;

        for (ItemStack content : plugin.getShopManager().removeNull(inv))
            if (plugin.getEconomyConfig().hash(content).equals(s))
                actual += content.getAmount();

        return actual >= amount;
    }

    private Inventory getChest(Block signBlock) {
        final BlockFace face = ((WallSign) signBlock.getState().getBlockData()).getFacing().getOppositeFace();
        final Location location = signBlock.getLocation();

        //final BlockFace signFace = reference.getFacing().getOppositeFace();

        switch (face) {
            case NORTH:
                location.setZ(location.getBlockZ() - 1);
                break;
            case SOUTH:
                location.setZ(location.getBlockZ() + 1);
                break;
            case WEST:
                location.setX(location.getBlockX() - 1);
                break;
            case EAST:
                location.setX(location.getBlockX() + 1);
                break;
            default:
                return null;
        }

        final Chest c = (Chest) Bukkit.getWorld("world").getBlockAt(location).getState();

        return c.getInventory();
    }
}
