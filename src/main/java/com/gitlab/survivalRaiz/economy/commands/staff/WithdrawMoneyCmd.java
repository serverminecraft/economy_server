package com.gitlab.survivalRaiz.economy.commands.staff;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class WithdrawMoneyCmd extends ConfigCommand {

    private final EconomyPlugin plugin;

    public WithdrawMoneyCmd(EconomyPlugin plugin) {
        super("retirar", "retira dinheiro ao jogador", "/retirar <jogador> [quantia]",
                new ArrayList<>(), "retirar");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length < 1)
            throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());

        final int syntax = strings.length == 1 ? 0 : 1;

        if (!Bukkit.getOfflinePlayer(strings[0]).hasPlayedBefore())
            throw new SRCommandException(commandSender, Message.PLAYER_NOT_FOUND, plugin.getCore());

        if (!(commandSender instanceof Player))
            if (syntax == 1)
                try{
                    Double.parseDouble(strings[1]);
                    return syntax;
                } catch (NumberFormatException e) {
                    throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());
                }

        final Player p = (Player) commandSender;

        if (!Perms.TAKE_MONEY.allows(p))
            throw new SRCommandException(p, Message.NO_PERMISSIONS, plugin.getCore());

        if (syntax == 1)
            try{
                Double.parseDouble(strings[1]);
            } catch (NumberFormatException e) {
                throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());
            }

        return syntax;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);
        final OfflinePlayer op = Bukkit.getOfflinePlayer(strings[0]);

        if (syntax == 0)
            plugin.getEconomy().withdrawPlayer(op.getUniqueId(), plugin.getEconomy().getBalance(op.getUniqueId()));
        else if (syntax == 1)
            plugin.getEconomy().withdrawPlayer(op.getUniqueId(), Double.parseDouble(strings[1]));

        // TODO: 12/26/20 Fix this when the new scoreboard API is implemented
        plugin.getCore().reloadScoreboard(op.getPlayer());
    }
}
