package com.gitlab.survivalRaiz.economy.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ShopCmd extends ConfigCommand {

    private final EconomyPlugin plugin;

    public ShopCmd(EconomyPlugin plugin) {
        super("loja", "cria uma loja na localização para onde está a olhar",
                "/loja q<quantidade> <c<preço de compra> | v<preço de venda>>",
                new ArrayList<>(), "loja");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (strings.length < 2)
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        // q <c|v> => 0 || q c v => 1
        final int syntax = strings.length == 2 ? 0 : 1;

        switch (syntax) {
            case 1:
                try {
                    Integer.parseInt(strings[0]);
                } catch (NumberFormatException e) {
                    throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());
                }
                getArgValue(p, strings[2]);
            case 0:
                getArgValue(p, strings[1]);
        }

        return syntax;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);
        final Player p = (Player) commandSender;

        final int q = Integer.parseInt(strings[0]);

        final double c = strings[1].startsWith("c") ? getArgValue(p, strings[1]) :
                (syntax == 1 && strings[2].startsWith("c")) ? getArgValue(p, strings[2]) : -1;
        final double v = strings[1].startsWith("v") ? getArgValue(p, strings[1]) :
                (syntax == 1 && strings[2].startsWith("v")) ? getArgValue(p, strings[2]) : -1;

        System.out.println(q + " " + c + " " + v + " ");

        final int result = this.plugin.getShopManager().createShop(p, c, v, q);

        switch (result) {
            case 0:
                // TODO: 12/26/20 Fix this when the new scoreboard API is implemented
                plugin.getCore().reloadScoreboard(p);
                return;
            case -1:
                throw new SRCommandException(p, Message.LOOK_AT_CHEST, plugin.getCore());
            case -2:
            case -3:
                throw new SRCommandException(p, Message.CHEST_WITH_MULTIPLE_ITEMS, plugin.getCore());
            case -4:
                throw new SRCommandException(p, Message.PLAYER_BROKE, plugin.getCore());
            case -5:
                throw new CommandException(p, "O bloco onde o sign deve aparecer é null", getUsage());
        }
    }

    private double getArgValue(Player p, String arg) throws CommandException, NumberFormatException {
        if (!(arg.startsWith("c") || arg.startsWith("v")))
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        final String num = arg.substring(1);

        return Double.parseDouble(num);
    }
}
