package com.gitlab.survivalRaiz.economy.commands.player;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.core.messages.MsgHandler;
import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class BalanceCmd extends ConfigCommand {

    private final EconomyPlugin plugin;

    public BalanceCmd(EconomyPlugin plugin) {
        super("dinheiro", "vê quanto dinheiro o jogador tem", "/dinheiro [jogador]",
                new ArrayList<>(), "dinheiro");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length == 0)
            if (commandSender instanceof Player)
                return 0;
            else
                throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final OfflinePlayer op = Bukkit.getOfflinePlayer(strings[0]);

        if (!op.hasPlayedBefore())
            throw new SRCommandException(commandSender, Message.PLAYER_NOT_FOUND, plugin.getCore());

        return 1;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);

        final MsgHandler msgHandler = plugin.getCore().getMsgHandler();
        final double balance = syntax == 0 ?
                plugin.getEconomy().getBalance(((Player) commandSender).getUniqueId()) :
                plugin.getEconomy().getBalance(Bukkit.getOfflinePlayer(strings[0]).getUniqueId());

        msgHandler.message(commandSender,
                Message.PLAYER_BALANCE,
                (str) -> str.replaceAll("%bal%", String.valueOf(balance)));
    }
}
